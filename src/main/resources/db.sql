create schema fileshare;
use fileshare;
--drop table files;
create table FILES (
	ID int NOT NULL AUTO_INCREMENT,
	FILENAME varchar(255) NOT NULL, 
	FILEDESCRIPTION varchar(200),
	FILECONTENTTYPE varchar(200),
	FILESIZE int, 
	FILECONTENT LONGBLOB,
	PRIMARY KEY (ID)
);

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';