package com.hcl.training.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.hcl.training.util.Message;

/**
 * @author Lingamurthy.CS
 * ControllerAdvice class to generically handle exceptions in controller classes
 */
@ControllerAdvice
public class GlobalControllerAdvice {

	private static final Logger logger = LoggerFactory.getLogger(GlobalControllerAdvice.class);

	/**
	 * @param e
	 * @return
	 * method to handle exceptions from all controller classes and respond with the error response
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Message> handleException(Exception e) {
		logger.error(e.getMessage(), e);
		Message msg = new Message();
		msg.setMessage(e.getMessage());
		msg.setStatus(500);
		return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
	}
}
