package com.hcl.training.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.training.service.FileShareService;
import com.hcl.training.util.File;
import com.hcl.training.util.FileShareConstants;
import com.hcl.training.util.Message;
import com.hcl.training.util.UploadResponseMessage;

/**
 * @author Lingamurthy.CS
 * Controller class of FileShareProject
 */
@Controller
@RequestMapping(value = "/file")
public class FileShareController {

	@Autowired
	FileShareService fileShareServiceImpl;
	private static final Logger logger = LoggerFactory.getLogger(FileShareController.class);

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 * Controller method to upload file and return the id
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<Message> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
		/* a File reference to store the received file */
		logger.info("Received uploadFile request");
		File newfile = null;

		if (!file.isEmpty()) {
			newfile = new File();
			newfile.setFilename(file.getOriginalFilename());
			newfile.setContentType(file.getContentType());
			newfile.setSize(file.getSize());
			newfile.setBytes(file.getBytes());
		}

		/* call saveFile() in Service */
		int id = fileShareServiceImpl.saveFile(newfile);

		/* reference to hold the return Message */
		UploadResponseMessage responseMsg = new UploadResponseMessage();
		responseMsg.setId(id);
		
		if (id == 0) {
			responseMsg.setMessage(FileShareConstants.FAILURE_MSG);
			responseMsg.setStatus(FileShareConstants.FAILURE_STATUS);
			return new ResponseEntity<>(responseMsg, HttpStatus.BAD_REQUEST);
		} else {
			responseMsg.setMessage(FileShareConstants.SUCCESS_MSG);
			responseMsg.setStatus(FileShareConstants.SUCCESS_STATUS);
			logger.info("uploadFile successful with id=" + id);
			return new ResponseEntity<>(responseMsg, HttpStatus.OK);
		}

	}

	/**
	 * @param id
	 * @return
	 * Controller method to delete the id by its id 
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Message> deleteFile(@PathVariable int id) {
		logger.info("Received deleteFile request for id=" + id);

		int result = fileShareServiceImpl.removeFile(id);
	
		Message msg = new Message();
		if (result == 0) {
			msg.setMessage(FileShareConstants.FAILURE_MSG);
			msg.setStatus(FileShareConstants.FAILURE_STATUS);
			return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);
		} else {
			msg.setMessage(FileShareConstants.SUCCESS_MSG);
			msg.setStatus(FileShareConstants.SUCCESS_STATUS);
			return new ResponseEntity<>(msg, HttpStatus.OK);
		}
	}

	/**
	 * @param id
	 * @param response
	 * @return
	 * Controller method to download file by its id
	 */
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloadFile(@PathVariable int id, HttpServletResponse response) {
		logger.info("Received downloadFile request for id=" + id);
		File resultFile = fileShareServiceImpl.getFile(id);
		// store the bytes to be sent as file
		byte[] documentBody = resultFile.getBytes();

		MultiValueMap<String, String> headers = new HttpHeaders();
		headers.add("charset", "utf-8");
		headers.add("Content-disposition", "attachment; filename=" + resultFile.getFilename());
		headers.add("Content-Type", resultFile.getContentType());
		return new ResponseEntity<>(documentBody, headers, HttpStatus.CREATED);
	}

}
