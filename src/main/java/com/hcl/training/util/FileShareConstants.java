package com.hcl.training.util;

public class FileShareConstants {
	public static final String SUCCESS_MSG = "SUCCESS";
	public static final String FAILURE_MSG = "FAILURE";
	public static final int SUCCESS_STATUS = 200;
	public static final int FAILURE_STATUS = 500;
}
