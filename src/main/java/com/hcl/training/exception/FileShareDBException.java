package com.hcl.training.exception;

public class FileShareDBException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileShareDBException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
