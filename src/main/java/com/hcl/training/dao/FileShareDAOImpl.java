package com.hcl.training.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Repository;

import com.hcl.training.exception.FileShareDBException;
import com.hcl.training.util.File;
import com.mysql.cj.jdbc.Blob;

/**
 * @author Lingamurthy.CS
 * Repository class to handle DB operations 
 */
@Repository
public class FileShareDAOImpl implements FileShareDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final Logger logger = LoggerFactory.getLogger(FileShareDAOImpl.class);
	
	/* (non-Javadoc)
	 * @see com.hcl.training.dao.FileShareDAO#insertFile(com.hcl.training.util.File)
	 * method to insert file in DB and return the newly generated id
	 */
	@Override
	public int insertFile(File file) {
		Blob blob = new Blob(file.getBytes(), null);

		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		try {
			jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
					PreparedStatement statement = con.prepareStatement(
							"INSERT INTO FILES (FILENAME, FILEDESCRIPTION, FILECONTENTTYPE, FILESIZE, FILECONTENT) VALUES(?, ?, ?, ?, ?)",
							Statement.RETURN_GENERATED_KEYS);
					statement.setString(1, file.getFilename());
					statement.setString(2, file.getDescription());
					statement.setString(3, file.getContentType());
					statement.setLong(4, file.getSize());
					statement.setBlob(5, blob);
					return statement;
				}
			}, generatedKeyHolder);
		} catch (DataAccessException dae) {
			logger.error("Upload file failed" + dae);
			throw new FileShareDBException("Upload file failed", dae);
		}
		return generatedKeyHolder.getKey().intValue();
	}

	/* (non-Javadoc)
	 * @see com.hcl.training.dao.FileShareDAO#deleteFile(int)
	 * method to delete file from db by id and return 1 on successful deletion
	 */
	@Override
	public int deleteFile(int id) {
		int result = 0;
		try {
			result = jdbcTemplate.update("DELETE FROM FILES WHERE ID = ?", id);
		} catch (DataAccessException dae) {
			logger.error("Delete file failed" + dae);
			throw new FileShareDBException("Delete file failed", dae);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.hcl.training.dao.FileShareDAO#retrieveFile(int)
	 * method to retrieve file from db by id
	 */
	@Override
	public File retrieveFile(int id) {
		File retrievedFile = null;

		try {
			retrievedFile = jdbcTemplate.queryForObject("SELECT * FROM FILES WHERE ID = ?", new Object[] { id },
					(rs, rowNum) -> {
						File file = new File();
						// to handle the file long bob content
						LobHandler lobHandler = new DefaultLobHandler();

						file.setId(rs.getInt("ID"));
						file.setFilename(rs.getString("FILENAME"));
						file.setDescription(rs.getString("FILEDESCRIPTION"));
						file.setContentType(rs.getString("FILECONTENTTYPE"));
						file.setSize(rs.getLong("FILESIZE"));
						file.setBytes(lobHandler.getBlobAsBytes(rs, "FILECONTENT"));
						return file;
					});
		} catch (EmptyResultDataAccessException erdae) {
			logger.error("No matching file found" + erdae);
			throw new FileShareDBException("No matching file found", erdae);
		} catch (DataAccessException dae) {
			logger.error("Retrieve file failed" + dae);
			throw new FileShareDBException("Retrieve file failed", dae);
		}

		return retrievedFile;
	}

}
