package com.hcl.training.dao;

import com.hcl.training.util.File;

public interface FileShareDAO {
	int insertFile(File file);
	int deleteFile(int id);
	File retrieveFile(int id);
}
