package com.hcl.training.service;

import com.hcl.training.util.File;

public interface FileShareService {
	public int saveFile(File file);
	public int removeFile(int id);
	public File getFile(int id);
}
