package com.hcl.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.training.dao.FileShareDAO;
import com.hcl.training.util.File;

/**
 * @author Lingamurthy.CS
 * Service class of FileShareProject
 */
@Service
public class FileShareServiceImpl implements FileShareService {

	@Autowired
	FileShareDAO fileShareDAOImpl;
	
	@Override
	public int saveFile(File file) {
		return fileShareDAOImpl.insertFile(file);
	}

	@Override
	public int removeFile(int id) {
		return fileShareDAOImpl.deleteFile(id);
	}

	@Override
	public File getFile(int id) {
		return fileShareDAOImpl.retrieveFile(id);
	}

}
